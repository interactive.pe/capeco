const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');
const CopyPlugin = require('copy-webpack-plugin');
const fs = require('fs')
const pages = fs.readdirSync(path.resolve(__dirname, 'src')).filter(fileName => fileName.endsWith('.html'))


module.exports = {
  entry: {
    app: "./src/app.js"
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].bundle.js"
  },
  module: {
    rules: [
      {
        test: /\.pug/,
        use: 'pug-loader'
      },
      {
        test: /\.styl/,
        use: [
          "style-loader",
          "css-loader",
          "stylus-loader"
        ]
      },
      {
         test: /\.css$/,
         use: [
           'style-loader',
           'css-loader',
         ],
       },
      {
        test: /\.jpe?g$|\.gif$|\.png$|\.svg$|\.woff$|\.ttf$|\.wav$|\.mp3$/,
        use: "file-loader"
      },
      {
        test: /\.js$|\.es6$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['env']
          }
        }
      }
    ]
  },
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    open: true
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'app',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/index.html'
    }),
    new HtmlWebpackPlugin({
      title: 'capacitaciones',
      filename: 'capacitaciones.html',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/capacitaciones.html'
    }),
    new HtmlWebpackPlugin({
      title: 'programa',
      filename: 'programa.html',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/programa.html'
    }),
    new HtmlWebpackPlugin({
      title: 'soluciones',
      filename: 'soluciones.html',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/soluciones.html'
    }),
    new HtmlWebpackPlugin({
      title: 'calculadora',
      filename: 'calculadora.html',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/calculadora.html'
    }),
    new HtmlWebpackPlugin({
      title: 'calculadora2',
      filename: 'calculadora2.html',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/calculadora2.html'
    }),
    new HtmlWebpackPlugin({
      title: 'avance',
      filename: 'avance.html',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/avance.html'
    }),
    new HtmlWebpackPlugin({
      title: 'solucion',
      filename: 'solucion.html',
      hash: true,
      alwaysWriteToDisk: true,
      template: './src/solucion.html'
    }),
    new CopyPlugin({
      patterns: [
        { from: './src/assets', to: 'assets' }
      ],
    })

  ]
};
